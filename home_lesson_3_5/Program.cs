﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ukraine;
using france;
using german;

namespace home_lesson_3_5
{
    class Program
    {
        static void Main(string[] args)
        {
            ukraine.Kiev k = new ukraine.Kiev();
            france.Paris p = new france.Paris();
            german.Berlin b = new german.Berlin();

            ComparePopulation cp = new ComparePopulation();
            try
            {
                cp[0] = new Cities(k.Name, k.Population);
                cp[1] = new Cities(p.Name, p.Population);
                cp[2] = new Cities(b.Name, b.Population);
            }
            catch(ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }

            cp.Sort();
            Console.WriteLine("City`s population from smallest to bigest:\n");
            foreach(var i in cp)
            {
                Console.WriteLine(i.ToString());
            }
        }
    }  
}
