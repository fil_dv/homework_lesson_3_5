﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace home_lesson_3_5
{
    class ComparePopulation : IEnumerable<Cities>, IEnumerator<Cities> 
    {
        private Cities[] _arrCity;
        int _position = -1;

        public ComparePopulation(int size = 3)
        {
            _arrCity = new Cities[size];
        }

        public int Length
        { 
            get
            {
                return _arrCity.Length;
            }
        }

        public void Sort()
        {
            Array.Sort<Cities>(_arrCity);           
        }

        public Cities this[int index]
        {
            get
            {
                if (index < 0 || index > _arrCity.Length) 
                {
                    throw new ArgumentOutOfRangeException();
                }   
                else
                {
                    return (Cities)_arrCity[index];                
                }
            }
            set
            {
                _arrCity[index] = (Cities)value;
            }
        }

        public IEnumerator<Cities> GetEnumerator()
        {
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Cities Current
        {
            get
            {
                try
                {
                    return _arrCity[_position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public void Dispose()
        {
           GC.SuppressFinalize(this);
        }
       
        object System.Collections.IEnumerator.Current
        {
            get { return Current; }
        }

        public bool MoveNext()
        {
            _position++;
            return (_position < _arrCity.Length);
        }

        public void Reset()
        {
            _position = -1;
        }
    }
}
