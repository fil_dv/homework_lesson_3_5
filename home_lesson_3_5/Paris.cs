﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace france
{
    public class Paris
    {
        public int Population { get; set; }
        public string Name { get; set; }

        public Paris()
        {
            Population = 2200000;
            Name = "Paris";
        }      
    }
}
