﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_3_5
{
    class Cities : IComparable, IComparable<Cities>
    {
        string _city { get; set; }
        int _population { get; set; }

        public Cities(string sity = null, int population = 0)
        {
            _city = sity;
            _population = population;
        }

        public override string ToString()
        {
            return "in " + _city + " = " + _population.ToString();
        }

        public int CompareTo(object obj)
        {
            Cities c = obj as Cities;
            if (c == null) throw new FormatException();
            else if (_population > c._population) return 1;
            else if (_population == c._population) return 0;
            else return -1;
        }

        public int CompareTo(Cities other)
        {
            if (_population > other._population) return 1;
            else if (_population == other._population) return 0;
            else return -1;
        }
    }
}
