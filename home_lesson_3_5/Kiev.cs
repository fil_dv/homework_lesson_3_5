﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ukraine
{
    public class Kiev
    {
        public int Population { get; set; }
        public string Name { get; set; }

        public Kiev()
        {
            Population = 2880000;
            Name = "Kiev";
        }
    }
}
